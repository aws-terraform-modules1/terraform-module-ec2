#output "instance_name" {
#  description = "(string) Name of the server."
#  value       = aws_instance.EC2_instance.tags_all
#}

output "instance_ami" {
  description = "(string) Name or ID of the image the server was created from."
  value       = aws_instance.EC2_instance.ami
}

output "instance_type" {
  description = "(map) Private Network the server shall be attached to. The Network that should be attached to the server requires at least one subnetwork. Subnetworks cannot be referenced by Servers in the Hetzner Cloud API. Therefore Terraform attempts to create the subnetwork in parallel to the server. This leads to a concurrency issue. It is therefore necessary to use depends_on to link the server to the respective subnetwork"
  value       = aws_instance.EC2_instance.instance_type
}

output "instance_id" {
  description = "(string) id of the ec2 instance"
  value = aws_instance.EC2_instance.id
}
