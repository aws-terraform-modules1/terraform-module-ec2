variable "instance_type" {
  type = string
  description = "(Required, string) the ec2 instance type such as t2.micro"
  default = "t2.micro"
}

variable "instance_ami" {
  type = string
  description = "(Required, string) each EC2 instance has to use an image such as ubuntu, and AWS assigns them a code, so go to AWS and get the AMI code for the image you want to use"
  default = "ami-0ec7f9846da6b0f61"
}

variable "instance_name" {
  type = string
  description = "(Required, string) whatever you want your EC2 instance to be called in AWS"
}

variable "subnet_id" {
  type = string
  description = "(Optional, string) The ID of the subnet that the EC2 instance is going to be attached to"
}